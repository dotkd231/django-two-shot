from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from receipts.models import ExpenseCategory, Receipt, Account
from django.shortcuts import redirect
from receipts.forms import (
    AccountCreateForm,
    CategoryCreateForm,
    ReceiptCreateForm,
)


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipt/list.html"
    context_object_name = "receiptlist"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipt/create.html"
    form_class = ReceiptCreateForm

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class ExpenseCategory_list(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipt/expense_list.html"
    context_object_name = "expense_category"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class Account_list(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipt/account_list.html"
    context_object_name = "account_"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class CreateExepenseCategory(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipt/expensecategory.html"
    form_class = CategoryCreateForm

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")


class CreateAccount(LoginRequiredMixin, CreateView):
    model = Account
    form_class = AccountCreateForm
    template_name = "receipt/create_account.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_listview")
