from django.urls import path

from receipts.views import (
    CreateAccount,
    CreateExepenseCategory,
    ExpenseCategory_list,
    ReceiptCreateView,
    ReceiptListView,
    Account_list,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/",
        ExpenseCategory_list.as_view(),
        name="list_categories",
    ),
    path(
        "accounts/",
        Account_list.as_view(),
        name="account_listview",
    ),
    path(
        "categories/create/",
        CreateExepenseCategory.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/",
        CreateAccount.as_view(),
        name="create_account",
    ),
]
