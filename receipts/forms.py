from django.forms import DateField, DateInput, ModelForm
from receipts.models import Account, ExpenseCategory, Receipt


class CustomDateInput(DateInput):
    input_type = "date"


class ReceiptCreateForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]


class CategoryCreateForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class AccountCreateForm(ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
